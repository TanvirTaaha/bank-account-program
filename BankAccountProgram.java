import java.io.BufferedReader;
import java.io.InputStreamReader;
// import java.util.Scanner;

public class BankAccountProgram {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        // Scanner scr = new Scanner(System.in);

        boolean exited = false;
        int numberOfCustomers = 0;
        Bank bank = new Bank();
        Customer[] customers = bank.getCustomers();
        while (!exited) {
            System.out.println("Please Enter your choise: ");
            System.out.println("1. Add Account for a Customer");
            System.out.println("2. Deposit Money");
            System.out.println("3. Withdraw Money");
            System.out.println("4. Check Balance");
            System.out.println("5. Calculate Interest");
            System.out.println("6. Exit");
            int choise = Integer.parseInt(bufferedReader.readLine());
            // int choise = scr.nextInt();

            switch (choise) {
                case 1:
                    System.out.println("Creating an account for new customer: ");
                    System.out.println(
                            "Please enter the initial amount you want to deposit in your account for first time: ");
                    double bal = Double.parseDouble(bufferedReader.readLine());
                    System.out.println("Please assign your account number: ");
                    String acc = bufferedReader.readLine();
                    Account account = new Account(bal, acc);
                    System.out.println("Please enter your name: ");
                    String name = bufferedReader.readLine();
                    // Customer customer = new Customer(name, account);
                    customers[numberOfCustomers++] = new Customer(name, account);

                    System.out.println("Account created successfully. Name: " + name + ", Account Number: " + acc
                            + ", Balance: " + bal + "$");

                    // for (int i = 0; i < numberOfCustomers; i++) {
                    // System.out.println("NAME: " + customers[i].getName());

                    // }
                    break;

                case 2:
                    if (numberOfCustomers == 0) {
                        System.err.println("No account found.");

                    } else {
                        System.out.println("Please enter the account number.");
                        String accNo = bufferedReader.readLine();
                        boolean found = false;

                        for (int i = 0; (i < numberOfCustomers) && (!found); i++) {
                            if (accNo.equals(customers[i].getAccount().getAccountNumber())) {
                                found = true;
                                System.out.println("Please enter the amount to deposit.");
                                customers[i].getAccount().deposit(Double.parseDouble(bufferedReader.readLine()));
                                break;
                            }
                        }
                        if (!found) {
                            System.err.println("Account Number not found.");
                        }
                    }
                    break;

                case 3:
                    if (numberOfCustomers == 0) {
                        System.err.println("No account found.");

                    } else {
                        System.out.println("Please enter the account number.");
                        String accNo = bufferedReader.readLine();
                        boolean found = false;

                        for (int i = 0; i < numberOfCustomers; i++) {
                            if (accNo.equals(customers[i].getAccount().getAccountNumber())) {
                                found = true;
                                System.out.println("Please enter the amount to withdraw.");
                                customers[i].getAccount().withdraw(Double.parseDouble(bufferedReader.readLine()));
                                break;
                            }
                        }
                        if (!found) {
                            System.err.println("Account Number not found.");
                        }
                    }
                    break;

                case 4:
                    if (numberOfCustomers == 0) {
                        System.err.println("No account found.");

                    } else {
                        System.out.println("Please enter the account number.");
                        String accNo = bufferedReader.readLine();
                        boolean found = false;

                        for (int i = 0; i < numberOfCustomers; i++) {
                            if (accNo.equals(customers[i].getAccount().getAccountNumber())) {
                                found = true;
                                System.out.println(
                                        "Your account balance: " + customers[i].getAccount().getBalance() + "$");
                                // bank.calculateInterest(customers[i]);
                                break;
                            }
                        }
                        if (!found) {
                            System.err.println("Account Number not found.");
                        }
                    }
                    break;

                case 5:
                    if (numberOfCustomers == 0) {
                        System.err.println("No account found.");

                    } else {
                        System.out.println("Please enter the account number.");
                        String accNo = bufferedReader.readLine();
                        boolean found = false;

                        for (int i = 0; i < numberOfCustomers; i++) {
                            if (accNo.equals(customers[i].getAccount().getAccountNumber())) {
                                found = true;
                                bank.calculateInterest(customers[i]);
                                break;
                            }
                        }
                        if (!found) {
                            System.err.println("Account Number not found.");
                        }
                    }
                    break;

                case 6:
                    exited = true;
                    break;

                default:
                    System.err.println("Invalid input. Please enter a correct digit.");
                    break;
            }
        }
        // scr.close();
    }
}

class Bank {
    private static double interestRate = 8.5;
    private static double transactionFees = 10;
    private Customer[] customers = new Customer[1000];

    public void calculateInterest(Customer customer) {
        Account a = customer.getAccount();
        double bal = a.getBalance();
        double interestAmount = bal * interestRate / 100; // For simplicity calculates for a steady deposit of one year
                                                          // time period.
        double totalBalance = bal + interestAmount;
        System.out.println(
                "Interest amount: " + interestAmount + "$, Total balance after adding interest: " + totalBalance + "$");

    }

    public double getInterestRate() {
        return interestRate;
    }

    public static double getTransactionFees() {
        return transactionFees;
    }

    public Customer[] getCustomers() {
        return customers;
    }
    // public Account createAccount(Customer customer, double bal) {

    // }
}

class Account {
    private double balance = 100;
    private String accountNumber;
    private boolean isFirstTime = true;

    Account(String accNo) {
        this(100, accNo);
    }

    Account(double bal, String accNo) {
        accountNumber = accNo;
        balance = (bal >= 100) ? bal : 100;
    }

    public void deposit(double howMuch) {
        if (howMuch > 0) {
            balance += howMuch;
            System.out.println(
                    howMuch + "$ is successfully deposited in your account. The new balance is " + balance + "$");
        } else {
            System.err.println("Please ensure the amount to be deposited is positive.");
        }
    }

    public void withdraw(double howMuch) {
        if (howMuch > 0) {
            double tempBalance = balance - howMuch;
            if (isFirstTime == true) {
                if (tempBalance >= 100) {
                    balance = tempBalance;
                    System.out.println(howMuch + "$ is withdrawn. New balacne is: " + balance + "$");
                } else {
                    System.err.println("Insufficient balance to remove " + howMuch + "$");
                }
                isFirstTime = false;
            } else {
                tempBalance -= Bank.getTransactionFees();
                if (tempBalance >= 100) {
                    balance = tempBalance;
                    System.out.println(howMuch + "$ is withdrawn. Transaction fee " + Bank.getTransactionFees()
                            + "$ has been cut. New balacne is: " + balance + "$");
                } else {
                    System.err.println("Insufficient balance to remove " + howMuch + "$");
                }
            }
        } else {
            System.err.println("Please ensure the amount to e withdrawn is positive.");
        }
    }

    public double getBalance() {
        return balance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }
}

class Customer {
    private String name;
    private Account account;

    Customer(String name, Account acc) {
        this.name = name;
        this.account = acc;
    }

    public void display() {
        System.out.println("Name: " + name + ", Account Number: " + account.getAccountNumber() + ", Balance: "
                + account.getBalance() + "$");
    }

    public String getName() {
        return name;
    }

    public Account getAccount() {
        return account;
    }
}
